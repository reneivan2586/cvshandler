package com.availity.model;

import com.availity.utils.Constant;

/**
 * 
 * @author Rene Rodriguez
 *
 */

public class Enrollee {
	
	private final String userId;
	private final String firstName;
	private final String lastName;
	private final int version;
	private final String insuranceCompany;

	private Enrollee(String[] splitted) {
		userId = splitted[0];
		firstName = splitted[1];
		lastName = splitted[2];
		version = Integer.valueOf(splitted[3]);
		insuranceCompany = splitted[4];
	}
	
	public static Enrollee factoryEnrollees(String line) {
		if (line == null || line.trim().equals(Constant.EMPTY))
			return null;
		String[] splitted = line.split(Constant.COMMA);
		if (splitted.length < 5)
			return null;
		return new Enrollee(splitted);
	}

	

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @return the insuranceCompany
	 */
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Enrollee))
			return false;
		Enrollee newEnrollee = (Enrollee) obj;
		return this.insuranceCompany.equals(newEnrollee.insuranceCompany) && this.userId.equals(newEnrollee.userId)
				&& this.version == newEnrollee.version;
	}

	public boolean equalsWithoutVersion(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Enrollee))
			return false;
		Enrollee newEnrollee = (Enrollee) obj;
		return this.insuranceCompany.equals(newEnrollee.insuranceCompany) && this.userId.equals(newEnrollee.userId);
	}

	@Override
	public int hashCode() {
		return insuranceCompany.hashCode() * userId.hashCode() * version;
	}
	
}
