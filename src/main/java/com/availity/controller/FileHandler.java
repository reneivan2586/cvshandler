package com.availity.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.availity.service.EnrolleeService;
import com.availity.utils.Validate;
import com.availity.utils.Constant;
import com.availity.model.Enrollee;
import com.availity.utils.*;

public class FileHandler {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println(Constant.INPUT_SOURCE_PATH);
		String arg = input.nextLine();
		System.out.println(Constant.INPUT_DESTINATION_PATH);
		String arg1 = input.nextLine();
		
		
		try {
		
				
		// Validate required input parameters
		if (!Validate.parameters(arg,arg1))
			return;
		// Validate source path [REQUIRED]
		if (!Validate.path(arg, Constant.SOURCE))
			return;
		// Validate destination path
		if (!Validate.path(arg1, Constant.DESTINATION))
			return;

		// Get the parameters
		Path source = Paths.get(arg);
		Path destination = Paths.get(arg1);

		// Load files data in memory
		List<Enrollee> enrollees = FileUtility.load(source);
		
		// Process records 
		Map<String, List<Enrollee>> enrolleesProcessed = EnrolleeService.process(enrollees);

		// Sort the records based on the documentation requirement
		EnrolleeService.sort(enrolleesProcessed);

		// Save the result into a destination file.
		FileUtility.save(destination, enrolleesProcessed);
		}
		catch (Exception e) {
			System.out.println(Constant.FAIL + e.getMessage());
		}
		finally {
			System.out.println(Constant.SUCCESS);
			input.close();
		}

	}

}
