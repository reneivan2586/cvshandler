package com.availity.utils;

import static java.util.logging.Level.SEVERE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * @author Rene Rodriguez
 *
 */
public class Validate {

	private static final Logger LOGGER = Logger.getLogger(Validate.class.getName());
	

	/**
	 * Validate if the required parameters were provided by the customer 
	 * @param arg
	 * @param arg1
	 * @return
	 */
	public static boolean parameters(String arg, String arg1) {
		boolean validationWithSuccess = true;
		if ((arg == null || arg.trim().equals("")) && (arg1 == null || arg1.trim().equals(""))) {

			System.out.println(Constant.FIELDS_REQUIRED);
			LOGGER.log(SEVERE, Constant.FIELDS_REQUIRED);
			validationWithSuccess = false;
		}
		return validationWithSuccess;

	}
	
	/**
	 * Validate if the path provided by the user are valid or not
	 * @param arg
	 * @param input
	 * @return
	 */
	
	public static boolean path(String arg, String input)
	{
		boolean validationWithSuccess = true;
		Path path = Paths.get(arg);
		if (path == null || Files.notExists(path)) {
			String msg = input.equalsIgnoreCase(Constant.DESTINATION) ? Constant.DESTINATION : Constant.SOURCE;
			msg = String.format(msg, arg);
			System.out.println(msg);
			LOGGER.log(SEVERE, msg);
			validationWithSuccess = false;
		}
		return validationWithSuccess;
	}

}
