package com.availity.utils;

import static java.util.logging.Level.SEVERE;
import com.availity.model.Enrollee;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.stream.Stream;


/**
 * @author RENE RODRIGUEZ
 *
 */

public class FileUtility {

	private static final Logger LOGGER = Logger.getLogger(FileUtility.class.getName());

	/**
	 * @param source
	 * @return
	 */
	public static List<Enrollee> load(Path source) {
		if (source == null)
			return new ArrayList<>();
		try (Stream<String> stream = Files.lines(source)) {
			List<Enrollee> enrollees = new ArrayList<>();
			stream.forEach(l -> enrollees.add(Enrollee.factoryEnrollees(l)));
			return enrollees;
		} catch (IOException e) {
			LOGGER.log(SEVERE, e.getMessage(), e);
		}
		return new ArrayList<>();
	}

	/**
	 * @param path
	 * @param enrollees
	 * @return
	 */
	public static List<Path> save(Path path, Map<String, List<Enrollee>> enrollees) {
		List<Path> result = new ArrayList<>();
		Iterator<Entry<String, List<Enrollee>>> iterator = enrollees.entrySet().iterator();

		while (iterator.hasNext()) {
			Entry<String, List<Enrollee>> entry = iterator.next();

			Path exitPath = Paths.get(path.toAbsolutePath() + File.separator + entry.getKey() + Constant.UNDER_SCORE
					+ Constant.TIME_STTAMP_PATTERN.format(LocalDateTime.now()) + Constant.CSV_EXTENSION);

			try (PrintWriter pw = new PrintWriter(
					Files.newBufferedWriter(exitPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE))) {
				entry.getValue().forEach(e -> pw.println(e.getUserId() + Constant.COMMA + e.getFirstName() + Constant.COMMA
						+ e.getLastName() + Constant.COMMA + e.getVersion() + Constant.COMMA + e.getInsuranceCompany()));
				result.add(exitPath);

			} catch (IOException e) {
				System.out.println(Constant.FILE_CANT_BE_CREATED);
				LOGGER.log(SEVERE, Constant.FILE_CANT_BE_CREATED, e);
			}
		}
		return result;
	}

}
