package com.availity.utils;

import java.time.format.DateTimeFormatter;

/**
 * @author Rene Rodriguez
 *
 */
public class Constant {
	
	/**	
	 *  All application being used in the project 
	 * 
	 * 
	 */
		
	/*Error Messages */ 
	public static final String FILE_CANT_BE_CREATED = "File can't be created in the specified path";
	public static final String FIELDS_REQUIRED = "Source and destination folders are required";
    public static final String DESTINATION_PATH_DOES_NOT_EXIST = "Destination path does not  exist. Path: %s";
    public static final String SOURCE_PATH_DOES_NOT_EXIST = "Source path does not  exist. Path: %s";
    
    /*Utility */
    public static final DateTimeFormatter TIME_STTAMP_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss");
	public static final String CSV_EXTENSION = ".CSV";
	public static final String UNDER_SCORE = "_";
	public static final String COMMA = ",";
	public static final String EMPTY = "";
     
    
    /*INFO Messages */
    public static final String DESTINATION = "Destination";
	public static final String SOURCE = "SOURCE";
	public static final String INPUT_SOURCE_PATH = "Please enter the source file path";
	public static final String INPUT_DESTINATION_PATH  = "Please enter the destination file path";
	public static final String SUCCESS = "CSV File processed susessfully!!";
	public static final String FAIL = "Process is failed: ";
	
	
}
