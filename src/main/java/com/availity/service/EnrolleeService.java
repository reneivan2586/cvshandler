/**
 * 
 */
package com.availity.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.availity.model.*;


/**
 * @author Rene Rodriguez
 *
 */

public class EnrolleeService {

	/**
	 * and separate enrollees by insurance company in its own file 
	 * 
	 * @param  enrollees  
	 * @return Map structure where the key is the Insurance Company and the value is theirs enrollees 
	 * 
	 */
	
	public static Map<String, List<Enrollee>> process(List<Enrollee> enrollees) {
		Map<String, List<Enrollee>> companies = new HashMap<>();

		for (Enrollee enrollee : enrollees) {
			List<Enrollee> company = companies.get(enrollee.getInsuranceCompany());
			if (company == null) {
				company = new ArrayList<Enrollee>();
				company.add(enrollee);
				companies.put(enrollee.getInsuranceCompany(), company);
			} else {
				add(company, enrollee);
			}
		}
		return companies;
	}

	/**
	 *  Add a new enrollee to the corresponding Insurance company
	 *  if there are duplicate User Ids for the same Insurance Company, then only the record with the highest version should be included.
	 *  
	 * @param currentInsuranceCompany
	 * @param newEnrollee
	 */
	public static void add(List<Enrollee> currentInsuranceCompany, Enrollee newEnrollee) {
		Optional<Enrollee> current = currentInsuranceCompany.stream().filter(e -> e.equalsWithoutVersion(newEnrollee))
				.findFirst();
		if (current.isPresent()) {
			if (newEnrollee.getVersion() > current.get().getVersion()) {
				currentInsuranceCompany.remove(current.get());
				currentInsuranceCompany.add(newEnrollee);
			}
		} else {
			currentInsuranceCompany.add(newEnrollee);
		}
	}

	/**
	 * Sort the contents of each file by last and first name (ascending)
	 * @param enrolleesProcessed
	 */
	public static void sort(Map<String, List<Enrollee>> enrolleesProcessed) {
		Comparator<Enrollee> comparedByLastNamesFirstName = Comparator.comparing(Enrollee::getLastName)
				.thenComparing(Enrollee::getFirstName);
		
		for (Map.Entry<String,List<Enrollee>> entry : enrolleesProcessed.entrySet())
			Collections.sort(entry.getValue(), comparedByLastNamesFirstName);
		
		};
	
}
