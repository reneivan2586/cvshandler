/**
 * 
 */
package com.availity.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.availity.model.*;
import com.availity.service.*;
import com.availity.utils.FileUtility;


/**
 * @author RIRO
 *
 */
class CsvHandlerTest {

	private static final Path CSV_PATH = Paths.get("src/test/resources/enrollmentFileTest.csv");

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void addSameVersion() {
		List<Enrollee> listEnrolles = FileUtility.load(CSV_PATH);
		Enrollee newEnrolle = Enrollee.factoryEnrollees("aaaa,Rene,Reodriguez,1,Infinity");
		EnrolleeService.add(listEnrolles, newEnrolle);
		assertEquals(listEnrolles.size(), 19);
	}

	@Test
	public void addSuperiorVersion() {
		List<Enrollee> listEnrolles = FileUtility.load(CSV_PATH);
		Enrollee newEnrolle = Enrollee.factoryEnrollees("aaaa,Rene,Rodriguez,19,Infinity");
		EnrolleeService.add(listEnrolles, newEnrolle);
		assertTrue(listEnrolles.contains(newEnrolle));

	}

	@Test
	public void addInferiorVersion() {
		List<Enrollee> listEnrolles = FileUtility.load(CSV_PATH);
		Enrollee newEnrolle = Enrollee.factoryEnrollees("aaaa,Rene,Reodriguez,0,Infinity");
		EnrolleeService.add(listEnrolles, newEnrolle);
		assertTrue(!listEnrolles.contains(newEnrolle));

	}

	@Test
	public void testLoadFile() {
		List<Enrollee> result = FileUtility.load(CSV_PATH);
		assertNotEquals(result, null);
		assertEquals(result.size(), 19);
	}

	@Test
	public void testAddPath() {
		List<Enrollee> listEnrolles = FileUtility.load(CSV_PATH);
		Enrollee newEnrolle = Enrollee.factoryEnrollees("uuuu,Miller,David,56,Blue Shield");
		EnrolleeService.add(listEnrolles, newEnrolle);
		assertEquals(listEnrolles.size(), 20);
	}

	@Test
	public void process() {
		List<Enrollee> enrollees = FileUtility.load(CSV_PATH);
		Map<String, List<Enrollee>> enrolleesProcessed = EnrolleeService.process(enrollees);
		assertTrue(enrolleesProcessed.size() == 4);

	}

	@Test
	public void testSortEnrollee() {
		List<Enrollee> enrollees = FileUtility.load(CSV_PATH);
		Map<String, List<Enrollee>> enrolleesProcessed = EnrolleeService.process(enrollees);
		EnrolleeService.sort(enrolleesProcessed);
		String firstName = enrolleesProcessed.get("Datapro").get(0).getFirstName();
		String lastName = enrolleesProcessed.get("Geico").get(1).getLastName();
		Integer version = enrolleesProcessed.get("Infinity").get(6).getVersion();
		String firstNameV1 = enrolleesProcessed.get("StateFarm").get(0).getFirstName();
		assertTrue("Ana".equals(firstName));
		assertTrue("Rodriguez".equals(lastName));
		assertTrue(version.equals(4));
		assertTrue("Yoel".equals(firstNameV1));

	}

	@Test
	public void Save() {
		
		List<Enrollee> enrollees = FileUtility.load(CSV_PATH);
		Map<String, List<Enrollee>> enrolleesProcessed = EnrolleeService.process(enrollees);
		EnrolleeService.sort(enrolleesProcessed);
		Path dir = Paths.get(System.getProperty("java.io.tmpdir"));
        Integer size = Integer.valueOf("4");
		List<Path> files = FileUtility.save(dir, enrolleesProcessed);
		assertTrue(size.equals(files.size()));
				
		files.stream().forEach(t -> {
			try {
				Files.deleteIfExists(t);
			} catch (IOException ignored) {
			}
		});
	}

}
