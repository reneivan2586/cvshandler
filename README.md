6.	Coding exercise:  Availity receives enrollment files from various benefits management and enrollment solutions (I.e. HR platforms, payroll platforms).  
Most of these files are typically in EDI format.  However, there are some files in CSV format.  For the files in CSV format, write a program in a language 
that seems appropriate to you that will read the content of the file and separate enrollees by insurance company in its own file. Additionally, sort the 
contents of each file by last and first name (ascending).  Lastly, if there are duplicate User Ids for the same Insurance Company, then only the record with 
the highest version should be included. The following data points are included in the file:
•	User Id (string)
•	First and Last Name (string)
•	Version (integer)
•	Insurance Company (string)



How to run:
            
   1- Open a command prompt with admin priviledge.
   2- run this command java -jar [projectPath]/csvhandler/distribute/csvhandler-Release-1.0.jar
   3- Introduce the path to the cvs file with the data. Example:   C:/Users/rene/Desktop/input/Test.csv
   4- Introduce the output path desired. Example:  C:/Users/rene/Desktop/output
   5- A message in the console will show the result of the process.(Success or fail)
   6- Review the output directory.   